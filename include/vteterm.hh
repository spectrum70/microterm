#ifndef vteterm_hh
#define vteterm_hh

#include <wx/control.h>
#include <string>
#include <gtk/gtk.h>
#include <vte/vte.h>

using std::string;

class wxWindow;

class vte_term : public wxControl {
public:
        vte_term();
        vte_term(wxWindow *parent,
                  wxWindowID id,const wxString &label,
                  const wxPoint &pos = wxDefaultPosition,
                  const wxSize &size = wxDefaultSize,
                  long style = 0,
                  const wxString &name = "");

        void set_buffer_lines(int lines);
        int  get_char_width();
        int  get_char_height();
        void char_to_hex (unsigned char d, char *rbuff);

        // overriden base virtuals
        virtual bool HasTransparentBackground() { return false; }

        void write(const char *buff, int len);
        void write(const string& str);
        void write(char c);
        void write_data_char (unsigned char d);
        void clear();
        void copy_to_clipboard();
        void paste_clipboard();
        bool has_selection();
	void select_all();
	void hide_cursor(bool = true);
	GtkWidget * get_terminal() { return (GtkWidget *)term; }

private:
        virtual void DoSetSize(int x, int y, int width, int height,
					int sizeFlags = wxSIZE_AUTO);
        virtual wxSize DoGetBestSize() const;
	void set_weight_by_sgr(const string &weight);
        string wx_font_string_to_pango(const string &f);

public:
        void set_colours(const char *fg, const char *bg);
        void set_font(const string &f);
private:
        VtePty   *pty;
        unsigned int h_key_press;
        VteTerminal *term;
        GdkRGBA *f,*b;
};

#endif // vteterm_hh
