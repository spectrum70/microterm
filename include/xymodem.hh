#ifndef __xymodem_hh
#define __xymodem_hh

#include "thread.hh"
#include <string>
#include <fstream>

using std::fstream;
using std::string;

// bitmask
enum {
	XMODEM_STD,
	XMODEM_1K,
	XMODEM_CRC,
	YMODEM_STD,
};

class __tools;

class com_port;

struct y_modem_data
{
	y_modem_data() : first_ack(false) {}

	bool first_ack;
	bool first_pkt;
};

class dispatcher : public thread
{
public:
	dispatcher(com_port &c, wxEvtHandler *eh);
	virtual ~dispatcher() {}
private:
	wxThread::ExitCode Entry();

	void setup_info_block(string &block);
	void update_progress(int = 0);
	void print_transfer_info();

	bool get_char();
	bool is_char();
	bool is_ack();
	bool is_sync();
	bool is_nack();
	bool is_can();

protected:
	void send_block(char *p, int size);
	void send_eot();

	virtual int get_block_size() = 0;
	virtual int setup_checksum_crc(char *data, char *dest, int size) = 0;
	virtual char get_sync() = 0;
	virtual char get_ack() = 0;
	virtual char get_can() = 0;
	virtual char get_nack() = 0;
	virtual char get_eot() = 0;
	virtual int get_proto() = 0;

	virtual void set_size(int) {}
	virtual void set_mode(int) {}
	virtual int get_mode() { return 0; }

public:
	void session_startup(const string &);
	void start_transfert();

protected:
	com_port *com, *q;
	fstream f;
	int pkt;
	int fsize, psize;
	int count;
	string fname;
	string ibuff;
	string obuff;
	int dpg_ptr;
};

class generic_proto
{
public:
	generic_proto();
	~generic_proto();
protected:
	__tools *t;
	int blk_size;
	int proto;
};

static const int LEN_HDR = 3;
static const int LEN_B_XMODEM = 128;
static const int LEN_B_XMODEM_1K = 1024;

class xmodem : public generic_proto, public dispatcher
{
public:
	xmodem(com_port &c, wxEvtHandler *eh, int proto_variant = XMODEM_STD);

private:
	int get_block_size() { return blk_size; }
	int setup_checksum_crc(char *data, char *dest, int size);

	char get_sync();
	char get_ack();
	char get_nack();
	char get_can();
	char get_eot();
	int get_proto() { return proto; }

	void set_size(int size);
	void add_mode(int mode) { proto = mode; }
};

/* we don't use the standard 128 block but 1024 only
 */
static const int LEN_B_YMODEM = 1024;

class ymodem : public generic_proto, public dispatcher
{
public:
	ymodem(com_port &c, wxEvtHandler *eh, int proto_variant = YMODEM_STD);

private:
	int get_block_size() { return blk_size; }
	int setup_checksum_crc(char *data, char *dest, int size);

	char get_sync();
	char get_ack();
	char get_nack();
	char get_can();
	char get_eot();
	int get_proto() { return proto; }

	void set_size(int size);
};

#endif /* __xymodem_hh */

