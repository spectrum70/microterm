#ifndef dlgpref_HH
#define dlgpref_HH

#include <wx/dialog.h>
#include <string>

using std::string;

class wxPropertyGrid;
class wxCommandEvent;

class DlgPref : public wxDialog
{
public:
   DlgPref ();

   void Init   ();

private:
   void SetupValues ();
   wxFont GetFontFromConfig(const wxString & cfgstr);

   void OnOk (wxCommandEvent &);

private:
   wxPropertyGrid *pg;

private:
   DECLARE_EVENT_TABLE()
};

#endif // dlgpref_HH
