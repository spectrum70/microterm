#ifndef THREAD_HH
#define THREAD_HH

#include <wx/thread.h>

class wxEvtHandler;

enum evtid {
	evt_id_log = 2000,
	evt_id_end,
	evt_id_del,
};

class thread: public wxThread
{
protected:
	wxEvtHandler* m_parent;

public:
	thread(wxEvtHandler* pParent) : wxThread(), m_parent(pParent) {}

	void start();

	/* events for UI logging */
	void evt_msg(const wxString &sz);
	void evt_dbg(const wxString &sz);
	void evt_end();
	void evt_del();

protected:
	virtual wxThread::ExitCode Entry() = 0;
};

#endif // THREAD_HH
