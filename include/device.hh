#ifndef device_hh
#define device_hh

#include <cstdint>
#include <tuple>

using namespace std;

enum types {
	t_serial,
	t_usb,
	t_acm,
	t_max,
};

enum states {
	s_invalid = 0,
	s_exist = (1 << 0),
	s_owned = (1 << 1),
	s_open = (1 << 2),
	s_new = (1 << 3),
};

constexpr int max_devs = 8;

class device {
public:
	device();

	void set_in_use(enum types, int num);
	void get_device_states();
	bool get_any_changes(tuple<uint64_t, uint64_t, uint64_t> &t) {
		t = make_tuple(*(uint64_t *)state[t_serial],
			       *(uint64_t *)state[t_usb],
			       *(uint64_t *)state[t_acm]);

		return (*(uint64_t *)state[t_serial] !=
			*(uint64_t *)state_old[t_serial] ||
			*(uint64_t *)state[t_usb] !=
			*(uint64_t *)state_old[t_usb] ||
			*(uint64_t *)state[t_acm] !=
			*(uint64_t *)state_old[t_acm]);
	}
	char *get_state(enum types t) { return state[t]; }

private:
	char state[t_max][max_devs] {};
	char state_old[t_max][max_devs] {};
};

#endif // device_hh
