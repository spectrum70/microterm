#ifndef serial_hh
#define serial_hh

#include <string>
#include <sys/ioctl.h>
#include <termios.h>

using std::string;

enum com_speed {
	CS200,
	CS300,
	CS600,
	CS1200,
	CS2400,
	CS4800,
	CS9600,
	CS19200,
	CS38400,
	CS57600,
	CS115200,
	CS230400,
};

enum com_parity {
	CPODD,
	CPEVEN,
	CPNONE,
};

struct com_config {
	string port;
	com_speed baudrate;
	com_parity parity;
	long databits;
	long stopbits;
};

class com_port {
public:
	com_port(com_port &);
	com_port(int number, com_speed speed, com_parity parity, int data_bits, int stop_bits);
	com_port(const char *port,
		 com_speed speed,
		 com_parity parity,
		 int data_bits,
		 int stop_bits);
	~com_port();

        void send_break();
        bool toggle_dtr();
        bool toggle_rts();
        int  get_modem_flags();
	int write(const char *buff, int len);
	int read(char *buff, int max_size);
	string get_speed_as_string(com_speed s);
	string get_parity_as_string(com_parity p);

	bool state_dtr(int flags) { return flags & TIOCM_DTR; }
        bool state_rts(int flags) { return flags & TIOCM_RTS; }
        bool state_dcd(int flags) { return flags & TIOCM_CAR; }
        bool state_dsr(int flags) { return flags & TIOCM_DSR; }
        bool state_rng(int flags) { return flags & TIOCM_RNG; }
        bool state_cts(int flags) { return flags & TIOCM_CTS; }
	bool is_open() {return (bool)(fd != -1); }
	int get_fd() { return fd; }
	int get_speed_value() { return speed_val; }

	int speed_to_int (com_speed speed)
	{
		switch (speed) {
		case CS200: return 200;
		case CS300: return 300;
		case CS600: return 600;
		case CS1200: return 1200;
		case CS2400: return 2400;
		case CS4800: return 4800;
		default:
		case CS9600: return 9600;
		case CS19200: return 19200;
		case CS38400: return 38400;
		case CS57600: return 57600;
		case CS115200: return 115200;
		case CS230400: return 230400;
		}

		return 0;
	}

private:
	void init_port(const char *port, com_speed speed, com_parity parity,
                        int data_bits, int stop_bits);
	int translate_speed(com_speed s);
	int translate_parity(com_parity p);
private:
	int fd;
	int speed_val;
	struct termios old_term;
};

#endif // serial_hh
