#ifndef dlgsend_HH
#define dlgsend_HH

#include <wx/dialog.h>
#include <string>

using std::string;

class wxButton;
class wxChoice;
class wxHtmlWindow;
class wxCommandEvent;

enum proto {
	p_raw,
	p_xmodem,
	p_xmodem_crc,
	p_xmodem_1k,
	p_ymodem,
	p_zmodem,
};

class dlg_send : public wxDialog
{
public:
	dlg_send() {}

	void init();

	string get_file() { return fname; }
	int get_proto() { return proto; }

private:
	void on_select_file(wxCommandEvent &);
	void on_protocol(wxCommandEvent &);

	void update_file_details();

private:
	wxButton *btnFile;
	wxButton *btnStart;
	wxButton *btnCancel;
	wxChoice *chProtocol;
	wxHtmlWindow *hwFile;

	string fname;
	int proto;

private:
	DECLARE_EVENT_TABLE()
};

#endif // dlgsend_HH
