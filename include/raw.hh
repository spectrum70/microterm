#ifndef RAW_HH
#define RAW_HH

#include "thread.hh"
#include <string>

using namespace std;

class wxWindow;
class com_port;

class RawSender : public thread {
public:
   RawSender (com_port &c, wxWindow *w, const string &);

private:
   wxThread::ExitCode Entry();

private:
   com_port  &cp;
   string   fname;

};

#endif
