#ifndef frmmain_hh
#define frmmain_hh

#include <wx/frame.h>
#include <wx/timer.h>
#include <wx/aui/auibar.h>
#include <gtk/gtk.h>
#include <string>
#include <sstream>
#include <map>
#include <set>

#include "dlgcustom.hh"
#include "device.hh"
#include "serial.hh"

using std::string;
using std::stringstream;
using std::map;
using std::set;

class wxStatusBar;
class wxMenu;
class wxToolBar;
class wxToolBarToolBase;
class wxCharEvent;
class wxTimerEvent;
class wxStaticText;

class com_port;
class vte_term;

class frm_main : public wxFrame
{
public:
	 frm_main();
	~frm_main();

private:
        void init_terminal();
	void init_terminal_window(bool first_init);
	void setup_status_bar(int client_width);
	void update_dce_flags(int modem_flags);
	void load_custom_tools();
	void attach_serial();
	void detach_serial();
	void restart_pty_master();
	void get_com_cfg();
	static void send_serial(GtkWidget *widget,
			   gchar *text, guint length, gpointer ptr);
	static gboolean recv_serial(GIOChannel* src,
				    GIOCondition cond, gpointer ptr);
	static gboolean recv_error(GIOChannel* src,
				    GIOCondition cond, gpointer ptr);
	static void recv_error_destroy(gpointer ptr); 
	void raw_tramsfert (const string &);
	void display_error(wxString &msg);

	void update_status(stringstream &ss, char *states, char sect);
	void update_serials_status();

	void add_ss_sect(stringstream &ss, char c);
	void add_ss(stringstream &ss, int val);
	void add_ss_new(stringstream &ss, int val);
	void add_ss_avail(stringstream &ss, int val);
	void add_ss_now_used(stringstream &ss, int val);

	void on_timer(wxTimerEvent &);
	void on_timer_dev(wxTimerEvent &);
	void on_send(wxCommandEvent&);
	void on_quit(wxCommandEvent&);
	void on_clear(wxCommandEvent&);
	void on_reopen(wxCommandEvent&);
	void on_view_data(wxCommandEvent&);
	void on_pref(wxCommandEvent&);
	void on_about(wxCommandEvent&);
	void on_send_break(wxCommandEvent&);
	void on_toggle_dtr(wxCommandEvent&);
	void on_toggle_rts(wxCommandEvent&);
	void on_custom_tool(wxCommandEvent&);
	void on_popup_click(wxCommandEvent&);
	void on_custom_command(wxCommandEvent&);
	void on_copy_to_clipboard(wxCommandEvent &);
	void on_paste_from_clipboard(wxCommandEvent &);
	void on_select_all(wxCommandEvent &);
	void on_key_down(wxKeyEvent &);
	void on_key_up(wxKeyEvent &);
	void on_size(wxSizeEvent &);
	void on_close(wxCloseEvent &);
	void on_term_right_click(wxMouseEvent &);
	void on_thread(wxCommandEvent& event);

	void get_physical_serials();

	DECLARE_EVENT_TABLE()

	string buff;
	bool rx_enabled, tx_enabled, ctrl;
	bool view_data;
        vect_cmds ccmds;
	wxMenu *popm;
	wxAuiToolBar *tool_bar;
	wxStatusBar *status_bar;
	int sh_keydown;
	
	guint callback_handler_in, callback_handler_err;
	vte_term *vte_terminal;
	com_port *com;
	wxStaticText* info;
	wxTimer tdev, tim;
	GIOChannel *ch_chars_in;
	wxPanel *panel;
	com_config ccfg;
	int cur_port_type;
	int cur_port_num;
	wxMenuItem * mi_view_data;
	wxAuiToolBarItem *tb_view_data;

	device dev;
};

#endif // frmmain_HH
