#ifndef __zmodem_hh
#define __zmodem_hh

#include <vector>
#include <cstdint>

#include "serial.hh"

using std::vector;

class wxEvtHandler;

struct crc
{
	uint16_t updcrc(uint8_t cp, uint16_t crc);
};

class zmodem : public crc
{
public:
	zmodem(com_port &c, wxEvtHandler *eh);

	int run();

	vector<uint8_t> data;
private:
	void pack_hdr_long(uint8_t *hdr, uint32_t val);
	void pack_binary_header(uint8_t frame_type, uint8_t *hdr);
	void pack_zesc_char(uint8_t c);

	int get_reply();

	void send_zrqinit();
	void send_zfile();
	void send_zdata();
	void send_zeof();
	void send_zfin();

private:
	com_port &cp;
};

#endif // __zmodem_hh
