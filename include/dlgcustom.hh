#ifndef dlgcustom_hh
#define dlgcustom_hh

#include <wx/dialog.h>
#include <vector>
#include <string>

using std::pair;
using std::vector;
using std::string;

class wxButton;
class wxBitmapButton;
class wxStyledTextCtrl;
class wxListBox;


typedef vector< pair<string, string> > vect_cmds;

class DlgCustom : public wxDialog
{
public:
        DlgCustom();

        void Init();

        vect_cmds GetCmdVector() { return cmds; }

private:
        void OnOk(wxCommandEvent &);

        void OnCommandNew(wxCommandEvent &);
        void OnCommandDelete(wxCommandEvent &);
        void OnCommandUp(wxCommandEvent &);
        void OnCommandDown(wxCommandEvent &);

        void OnListBox(wxCommandEvent &);
        void OnKillFocus(wxFocusEvent &);

        void MoveCommand (int delta);

        int  ValidateCmdName (const string &cmd);

        void LoadCustomCommands();
private:
        int selitem;

        wxButton *btnNew;
        wxButton *btnDelete;

        wxBitmapButton *btnUp;
        wxBitmapButton *btnDown;

        wxStyledTextCtrl *txtCommand;
        wxListBox  *lstCmds;

        vect_cmds cmds;

private:
        DECLARE_EVENT_TABLE()
};


#endif
