/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <wx/xrc/xmlres.h>
#include <wx/toolbar.h>
#include <wx/artprov.h>
#include <wx/statusbr.h>
#include <wx/menu.h>
#include <wx/button.h>
#include <wx/config.h>
#include <wx/msgdlg.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/event.h>
#include <wx/clipbrd.h>
#include <wx/dir.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/dc.h>
#include <sstream>

#include "frmmain.hh"
#include "xymodem.hh"
#include "zmodem.hh"
#include "raw.hh"
#include "serial.hh"
#include "dlgsend.hh"
#include "dlgpref.hh"
#include "vteterm.hh"
#include "icons.h"
#include "version.h"

static constexpr int max_buff_in = 512;
static constexpr int max_buff = (128*1024);
static constexpr int max_signals = 6;

static constexpr int ctrl_a = 0x01;
static constexpr int ctrl_c = 0x03;
static constexpr int ctrl_v = 0x16;

using std::stringstream;

enum ids {
	id_open = 101,
	id_exit,
	id_clear,
	id_reopen,
	id_view_data,
	id_pref,
	id_sendbreak,
	id_toggle_dtr,
	id_toggle_rts,
	id_about,
	id_copy,
	id_paste,
	id_select_all,
	id_custom,
	id_custom_command_first = 1000,
	id_custom_command_last = 1015,
};

/*
 * First field is status string, to be set later
 * max signals + status + spare
 */
static int st_widths[max_signals + 2] =
{0 /*to be set later*/, 32, 32, 32, 32, 32, 32, 32};

static int st_styles[max_signals + 2] = {
	wxSB_FLAT,    /* spare slot */
	wxSB_NORMAL,
	wxSB_NORMAL,
	wxSB_RAISED,
	wxSB_RAISED,
	wxSB_RAISED,
	wxSB_RAISED,
	wxSB_FLAT      /* spare slot */
};

/* first are the DTE enabled signals */
static const char signals[max_signals][4] =
	{"DTR","RTS","DCD","DSR","RNG","CTS"};

BEGIN_EVENT_TABLE(frm_main, wxFrame)
	EVT_COMMAND(wxID_ANY, wxEVT_THREAD, frm_main::on_thread)
	EVT_SIZE(frm_main::on_size)
	EVT_MENU(id_open, frm_main::on_send)
	EVT_MENU(id_exit, frm_main::on_quit)
	EVT_MENU(id_clear, frm_main::on_clear)
	EVT_MENU(id_reopen, frm_main::on_reopen)
	EVT_MENU(id_view_data, frm_main::on_view_data)
	EVT_MENU(id_copy, frm_main::on_copy_to_clipboard)
	EVT_MENU(id_paste, frm_main::on_paste_from_clipboard)
	EVT_MENU(id_select_all, frm_main::on_select_all)
	EVT_MENU(id_pref, frm_main::on_pref)
	EVT_MENU(id_sendbreak, frm_main::on_send_break)
	EVT_MENU(id_toggle_dtr, frm_main::on_toggle_dtr)
	EVT_MENU(id_toggle_rts, frm_main::on_toggle_rts)
	EVT_MENU(id_custom, frm_main::on_custom_tool)
	EVT_MENU(id_about, frm_main::on_about)
	EVT_MENU_RANGE(id_custom_command_first, id_custom_command_last,
				frm_main::on_custom_command)
	EVT_CLOSE(frm_main::on_close)
END_EVENT_TABLE()

class tb_art_provider: public wxAuiDefaultToolBarArt
{
public:
	tb_art_provider() {}
private:
	void DrawBackground (wxDC &dc, wxWindow *wnd, const wxRect &rect)
	{
		dc.SetBrush(*wxBLACK_BRUSH); // black filling
		dc.SetPen(wxColour(120, 120, 120)); // black filling
		dc.DrawRectangle(rect);
	}

};

frm_main::frm_main() : vte_terminal(0), com(0), tim(this, wxID_ANY), 
			ch_chars_in(0)
{
	buff.resize(max_buff);

	rx_enabled = true;
	ctrl = false;
	view_data = false;
	sh_keydown = 0;

	wxFont monospace(9, wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL,
			 wxFONTWEIGHT_NORMAL);

	wxXmlResource::Get()->LoadFrame(this, 0, wxT("frmMain"));

	wxAuiToolBarArt *art_prov =  new tb_art_provider();

	tool_bar = new wxAuiToolBar(this, wxID_ANY,
				    wxDefaultPosition,
				    wxDefaultSize);

	SetToolBar((wxToolBar*)tool_bar);

	tool_bar->SetArtProvider(art_prov);
	tool_bar->SetWindowStyleFlag(wxAUI_TB_GRIPPER);
	tool_bar->SetMargins(2, 2);

	status_bar = GetStatusBar();
	wxMenu* me;
	wxMenuItem *it;

	me = GetMenuBar()->GetMenu(0);
	it = new wxMenuItem (me, id_open,
			     "Upload file",
			     "Upload a file to remote device", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_exit, "Exit",
			     "Terminate the application", wxITEM_NORMAL);
	me->AppendSeparator();
	me->Append(it);
	me = GetMenuBar()->GetMenu(1);
	it = new wxMenuItem (me, id_clear, "Clear screen",
			     "Clear the terminal screen", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_copy, "Copy\tCTRL+C",
			     "Copy to clipboard", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_paste, "Paste\tCTRL+V",
			     "Paste clipboard", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_select_all, "Select All\tCTRL+A",
			     "Select all", wxITEM_NORMAL);
	me->Append(it);
	me->AppendSeparator();
	mi_view_data = new wxMenuItem (me, id_view_data, "View data",
			     "View as ASCII", wxITEM_CHECK);
	me->Append(mi_view_data);
	me->AppendSeparator();
	it = new wxMenuItem (me, id_custom, "Custom Tools",
			     "Create custom tools", wxITEM_NORMAL);
	me->Append(it);
	me->AppendSeparator();
	it = new wxMenuItem (me, id_pref, "Preferences",
			     "Application setup", wxITEM_NORMAL);
	me->Append(it);
	me = GetMenuBar()->GetMenu(2);
	it = new wxMenuItem (me, id_sendbreak, "Send break\tCTRL+B",
			     "Send a break", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_toggle_dtr, "Toggle DTR",
			     "Toggle DTR signal", wxITEM_NORMAL);
	me->Append(it);
	it = new wxMenuItem (me, id_toggle_rts, "Toggle RTS",
			     "Toggle RTS signal", wxITEM_NORMAL);
	me->Append(it);
	me = GetMenuBar()->GetMenu(3);
	it = new wxMenuItem (me, id_about, "About ...",
			     "About microterm", wxITEM_NORMAL);
	me->Append(it);

	/* in wxglade i'm enable toolbar in frame-properties dialog */
	tool_bar->AddTool(id_pref, "Preferences",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_settings)),
		    "Preferences");
	tool_bar->AddTool(id_open, "Upload",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_upload)),
		    "Upload file");
	tool_bar->AddTool(id_clear, "Clear",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_clear)),
		    "Clear the terminal");
	tool_bar->AddTool(id_reopen, "Reopen",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_refresh)),
                    "Reopen terminal");
	tool_bar->AddSeparator();
	tb_view_data = tool_bar->AddTool(id_view_data,
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_view_data)),
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_view_data)),
		    true,
		    0,
                    "View binary data");
	tool_bar->AddSeparator();
	tool_bar->AddTool(id_copy, "Copy",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_copy)),
                    "Copy");
	tool_bar->AddTool(id_paste, "Paste",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_paste)),
                    "Paste");
	tool_bar->AddTool(id_select_all, "Select All",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_select_all)),
                    "Select all");
	tool_bar->AddSeparator();
	tool_bar->AddTool(id_custom, "Setup Custom Commands",
		    wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_command_setup)),
                    "Setup custom commands");
	tool_bar->AddSeparator();
	tool_bar->Realize();

	vte_terminal = new vte_term(this, 10100, "",
				wxDefaultPosition, wxDefaultSize, 0, "");

	wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
	SetSizer(sizer);

	panel = new wxPanel(this, -1, wxDefaultPosition,
					wxDefaultSize);

	info = new wxStaticText(this, -1, "",
				wxDefaultPosition, wxSize(-1, 15),
				wxST_NO_AUTORESIZE);

	sizer->Add(vte_terminal, 1, wxEXPAND | wxALL, 0);
	sizer->Add(info, 0, wxEXPAND | wxALL, 0);
	sizer->Layout();

	sizer->Layout();
	Layout();
	this->Refresh();

	info->SetForegroundColour(wxColor(*wxYELLOW));
	info->SetBackgroundColour(wxColor(*wxBLACK));
	info->SetFont(monospace);

	status_bar->SetSize(wxSize(-1, 18));
	status_bar->SetFont(monospace);

	get_com_cfg();
	load_custom_tools();
	init_terminal();
	init_terminal_window(true);

	vte_terminal->Connect(wxEVT_RIGHT_DOWN, (wxObjectEventFunction)
		   &frm_main::on_term_right_click, NULL, this);

	popm = new wxMenu();
	it = new wxMenuItem(0, id_copy, "Copy\tCTRL+C");
	popm->Append(it);
	it = new wxMenuItem(0, id_paste, "Paste\tCTRL+V");
	popm->Append(it);
	it = new wxMenuItem(0, id_select_all, "Select all\tCTRL+A");
	popm->Append(it);

	dev.get_device_states();
	update_serials_status();

	tdev.Bind(wxEVT_TIMER, &frm_main::on_timer_dev, this);
	tdev.Start(1500);
}

frm_main::~frm_main()
{
	if (com)
		delete(com);
	delete(vte_terminal);
	delete(info);
	delete(popm);
}

void frm_main::get_com_cfg()
{
	wxConfig::Get()->SetPath("/Communication");

	ccfg.port = wxConfig::Get()->Read("devse");
	ccfg.baudrate = (com_speed)wxConfig::Get()->Read("baudr", 6);
	ccfg.parity = (com_parity)wxConfig::Get()->Read("parit", 2);
	ccfg.databits = wxConfig::Get()->Read("datab", 8);
	ccfg.stopbits = wxConfig::Get()->Read("stopb", 1);
}

void frm_main::add_ss_sect(stringstream &ss, char c)
{
	wxConfig::Get()->SetPath("/Info bar");

	wxColor bgcolor(wxConfig::Get()->Read("ib_bgcol_sect"));
	wxColor fgcolor(wxConfig::Get()->Read("ib_fgcol_sect"));

	ss << "<span color='"
		+ fgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"' background='"
		+ bgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"'> " << c << " </span>:";
}

void frm_main::add_ss_new(stringstream &ss, int val)
{
	wxConfig::Get()->SetPath("/Info bar");

	wxColor bgcolor(wxConfig::Get()->Read("ib_bgcol_new"));
	wxColor fgcolor(wxConfig::Get()->Read("ib_fgcol_new"));

	ss << "<span color='"
		+ fgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"' background='"
		+ bgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"'> " << val << " </span> ";
}

void frm_main::add_ss(stringstream &ss, int val)
{
	wxConfig::Get()->SetPath("/Info bar");

	wxColor bgcolor(wxConfig::Get()->Read("ib_bgcol_avail"));
	wxColor fgcolor(wxConfig::Get()->Read("ib_fgcol_avail"));

	ss << "<span color='"
		+ fgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"' background='"
		+ bgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"'> " << val << " </span> ";
}

void frm_main::add_ss_now_used(stringstream &ss, int val)
{
	wxConfig::Get()->SetPath("/Info bar");

	wxColor bgcolor(wxConfig::Get()->Read("ib_bgcol_owned"));
	wxColor fgcolor(wxConfig::Get()->Read("ib_fgcol_owned"));

	ss << "<span color='"
		+ fgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"' background='"
		+ bgcolor.GetAsString(wxC2S_HTML_SYNTAX) +
		"'> " << val << " </span> ";
}

void frm_main::update_status(stringstream &ss, char *states, char sect)
{
	int max = 8;

	add_ss_sect(ss, sect);

	/*
	 *Limit S display to 4, no-one has physical S now a day */
	if (sect == 'S')
		max = 4;

	for (int i = 0; i < max; ++i) {
		if (states[i] & s_exist && states[i] & s_new) {
			if (states[i] & s_owned) {
				add_ss_now_used(ss, i);
			} else {
				add_ss_new(ss, i);
			}
		} else if (states[i] & s_owned) {
			add_ss_now_used(ss, i);
		} else if (states[i] & s_exist) {
			if (states[i] & s_owned) {
				add_ss_now_used(ss, i);
			} else {
				add_ss(ss, i);
			}
		}
	}
}

void frm_main::update_serials_status()
{
	std::tuple<uint64_t, uint64_t, uint64_t> states;
	stringstream ss;

	if (dev.get_any_changes(states)) {
		/*
		 * Full recomposition of html must be done even
		 * if only one type have changes.
		 * But, avoid empty sections.
		 */
		if (std::get<t_serial>(states))
			update_status(ss, dev.get_state(t_serial), 'S');
		if (std::get<t_usb>(states))
			update_status(ss, dev.get_state(t_usb), 'U');
		if (std::get<t_acm>(states))
			update_status(ss, dev.get_state(t_acm), 'A');

		info->SetLabelMarkup(ss.str().c_str());
		info->Refresh();
	}
}

void frm_main::restart_pty_master()
{
	if (com)
		delete(com);

	/* Storing port details */
	cur_port_type = ccfg.port.c_str()[8];
	cur_port_num = (cur_port_type == 'S') ? ccfg.port.c_str()[9] :
						ccfg.port.c_str()[11];
	cur_port_num -= 48;

	com = new com_port(ccfg.port.c_str(),
				ccfg.baudrate,
				ccfg.parity,
				ccfg.databits,
				ccfg.stopbits);

	if (com->is_open()) {
		switch (cur_port_type) {
		case 'S':
			dev.set_in_use(t_serial, cur_port_num);
			break;
		case 'U':
			dev.set_in_use(t_usb, cur_port_num);
			break;
		case 'A':
			dev.set_in_use(t_acm, cur_port_num);
			break;
		}
		update_serials_status();
	}
}

void frm_main::display_error(wxString &msg)
{
	vte_terminal->write(msg.ToStdString());
	vte_terminal->hide_cursor(true);
}

void frm_main::init_terminal()
{
	restart_pty_master();

	vte_terminal->hide_cursor(false);

	if (!com->is_open()) {
		rx_enabled = false;

		wxString msg = wxString("error opening port ") + ccfg.port
				+ "\r\n";

		status_bar->SetStatusText(msg);

		vte_terminal->Enable(false);
		display_error(msg);
	} else {
		stringstream ss;

		attach_serial();

		ss << com->get_speed_as_string(ccfg.baudrate) << ","
		   << com->get_parity_as_string(ccfg.parity) << ","
		   << ccfg.databits << "," << ccfg.stopbits;

		status_bar->SetStatusText(wxString("Ready : ")
			+ ccfg.port + "," + ss.str());
	}
}

void frm_main::send_serial(GtkWidget *widget,
			   gchar *text, guint length, gpointer ptr)
{
	static wxCommandEvent e;
	frm_main *fm = (frm_main *)ptr;

	/*
	 * Handling here special CTRL accelrators, useful
	 * to be handled here. For now, not forwarding them.
	 */
	switch (*text) {
	case ctrl_a:
		fm->on_select_all(e);
		return;
	case ctrl_v:
		fm->on_paste_from_clipboard(e);
		return;
	case ctrl_c:
                fm->on_copy_to_clipboard(e);
	/* fall through */
	default:
		if (fm->com)
			fm->com->write(text, length);
		break;
	}
}

/*
 * On transferts or other operations (rx_enabled = false) we still
 * receive a first event but returning FALSE the Gsource is removed.
 * From that moment fd is available to be handled in xyzmodem stuff.
 */
gboolean frm_main::recv_serial(GIOChannel* src, GIOCondition cond, gpointer ptr)
{
	static gchar bin[max_buff_in];
	frm_main *fm = (frm_main *)ptr;

	if (fm->rx_enabled) {
		int len;

		if (fm->com) {
			len = fm->com->read(bin, max_buff_in);
			if (len > 0)
				fm->vte_terminal->write((char *)bin, len);
		}
		return TRUE;
	}

	return FALSE;
}

gboolean frm_main::recv_error(GIOChannel* src, GIOCondition cond, gpointer ptr)
{
	frm_main *fm = (frm_main *)ptr;

	fm->vte_terminal->write
		("\r\n\r\n"
		 "### RX ERROR - PORT HAS BEEN CLOSED, PLEASE REOPEN IT ###"
		 "\r\n\r\n");

	/* remove event after error, avoid 100% cpu loop */
	return 0;
}

void frm_main::recv_error_destroy(gpointer ptr)
{
	frm_main *fm = (frm_main *)ptr;

	g_source_remove(fm->callback_handler_in);
	g_source_remove(fm->callback_handler_err);
	g_io_channel_unref(fm->ch_chars_in);

	fm->detach_serial();

	if (fm->com) {
		delete(fm->com);
		fm->com = NULL;
	}
}

void frm_main::init_terminal_window(bool firs_tinit)
{
	wxConfig::Get()->SetPath("/Terminal");

	if (firs_tinit) {
		/* calculate minimal 80 x 24 */
		int x, y, w, h;

		w = wxConfig::Get()->Read("wwidth", 640);
		h = wxConfig::Get()->Read("wheight", 480);
		x = wxConfig::Get()->Read("wx", 20);
		y = wxConfig::Get()->Read("wy", 20);

		SetSize(w, h);
		SetPosition(wxPoint(x, y));
	}

	wxColor f(wxConfig::Get()->Read("fgcol"));
	wxColor b(wxConfig::Get()->Read("bgcol"));

	vte_terminal->clear();

	vte_terminal->set_colours(f.GetAsString(wxC2S_HTML_SYNTAX).ToAscii(),
		       b.GetAsString(wxC2S_HTML_SYNTAX).ToAscii());

	vte_terminal->set_font(
		(const char*)wxConfig::Get()->Read("font").ToAscii());
	vte_terminal->set_buffer_lines(wxConfig::Get()->Read("bufle", 5000));
}

void frm_main::load_custom_tools()
{
	/* Are there tools already ? */
	if (tool_bar) {
		int i = id_custom_command_first;
		while (i <= id_custom_command_last) {
			tool_bar->DeleteTool(i++);
		}
	}

	wxConfigBase &cfg = * wxConfig::Get();

	cfg.SetPath("/custom");

	long ids = id_custom_command_first;
	wxString e, value;
	long i;

	if (cfg.GetFirstEntry(e, i)) {
		do {
			string entry = (const char*)cfg.Read(e).c_str();

			int sep = entry.find(':');

			string name = entry.substr(0,sep);
			string cmd  = entry.substr(sep+1);

			pair <string, string> p (name, cmd);

			ccmds.push_back(p);

			tool_bar->AddTool (ids++, name,
				     wxBITMAP_PNG_FROM_DATA(command_2x), name);
		}
		while (cfg.GetNextEntry(e, i));
	}

	tool_bar->Realize();
}

/*
 * first field is status, 6 fields are signals, last is
 * just a blank space field.
 */
void frm_main::setup_status_bar(int client_width)
{
	st_widths[0] = client_width - (32 * max_signals) - 128;

	status_bar->SetFieldsCount(max_signals + 2);
	status_bar->SetStatusWidths(max_signals + 2, st_widths);
	status_bar->SetStatusStyles(max_signals + 2, st_styles);

	for (int i = 1; i < max_signals + 1; ++i)
		status_bar->SetStatusText(signals[i - 1], i);
}

void frm_main::update_dce_flags(int flags)
{
	st_styles[1] = (com->state_dtr(flags)) ? wxSB_SUNKEN : wxSB_RAISED;
	st_styles[2] = (com->state_rts(flags)) ? wxSB_SUNKEN : wxSB_RAISED;
	st_styles[3] = (com->state_dcd(flags)) ? wxSB_SUNKEN : wxSB_RAISED;
	st_styles[4] = (com->state_dsr(flags)) ? wxSB_SUNKEN : wxSB_RAISED;
	st_styles[5] = (com->state_rng(flags)) ? wxSB_SUNKEN : wxSB_RAISED;
	st_styles[6] = (com->state_cts(flags)) ? wxSB_SUNKEN : wxSB_RAISED;

	status_bar->SetStatusStyles(max_signals + 2, st_styles);
}

void frm_main::attach_serial()
{
	if (!sh_keydown) {
		sh_keydown = g_signal_connect_after(
				vte_terminal->get_terminal(),
				"commit",
				G_CALLBACK(send_serial), this);
	}

	vte_terminal->Enable(true);
	rx_enabled = true;

	ch_chars_in = g_io_channel_unix_new(com->get_fd());
	callback_handler_in = g_io_add_watch_full(ch_chars_in, 10, G_IO_IN,
				(GIOFunc)recv_serial, this, NULL);

	callback_handler_err = g_io_add_watch_full(ch_chars_in, 10, G_IO_ERR,
				(GIOFunc)recv_error, this,
				(GDestroyNotify)recv_error_destroy);
}

/*
 * Just disabling keypress here, rx is handled by rx_enabled = false
 */
void frm_main::detach_serial()
{
	vte_terminal->Enable(false);
	rx_enabled = false;
}

void frm_main::on_term_right_click (wxMouseEvent &evt)
{
	PopupMenu(popm);
}

void frm_main::on_custom_command (wxCommandEvent &evt)
{
	string s = (const char*)tool_bar->GetToolShortHelp(evt.GetId()).mb_str();

	if (s != "") {
		unsigned int i;
		for (i = 0; i < ccmds.size(); ++i) {
			pair<string,string> c = ccmds[i];

			if (c.first == s) {
				string cmd = c.second;

				com->write(cmd.c_str(), cmd.size());
				return;
			}

		}
	}
}

void frm_main::on_copy_to_clipboard(wxCommandEvent &evt)
{
	if (vte_terminal) {
		vte_terminal->copy_to_clipboard();
	}
}

void frm_main::on_paste_from_clipboard (wxCommandEvent &evt)
{
	if (vte_terminal) {
		wxClipboard cb;
		wxTextDataObject dobj;

		cb.Open();
		cb.GetData( dobj );
		cb.Close();
		com->write(dobj.GetText().c_str(), dobj.GetText().Length());
	}
}

void frm_main::on_select_all(wxCommandEvent &)
{
	if (vte_terminal)
		vte_terminal->select_all();
}

void frm_main::on_popup_click(wxCommandEvent &evt)
{
	switch(evt.GetId()) {
	case id_copy:
		on_copy_to_clipboard(evt);
		break;
	case id_paste:
		on_paste_from_clipboard(evt);
		break;
	}
}

void frm_main::on_send_break(wxCommandEvent &)
{
	com->send_break();
}

void frm_main::on_toggle_dtr(wxCommandEvent &)
{
	st_styles[1] = (com->toggle_dtr()) ? wxSB_SUNKEN : wxSB_RAISED;
	status_bar->SetStatusStyles(max_signals + 2, st_styles);
}

void frm_main::on_toggle_rts(wxCommandEvent &)
{
	st_styles[2] = (com->toggle_rts()) ? wxSB_SUNKEN : wxSB_RAISED;
	status_bar->SetStatusStyles(max_signals + 2, st_styles);
}

void frm_main::on_size(wxSizeEvent &e)
{
	if (vte_terminal) {
		wxSize csz=e.GetSize();

		setup_status_bar(csz.GetWidth());
		Layout();
	}
}

void frm_main::on_key_down(wxKeyEvent &e)
{
	static int c;

	c = e.GetKeyCode();
	switch(c) {
	case WXK_CONTROL:
		ctrl = true;
		/* no break */
	case WXK_SHIFT:
		return;
	default:
		break;
	}
	if (ctrl) {
		if (c == 'c') c = 0x03;
	}
	if (com->write((char*)&c, 1) == -1)
		wxMessageBox("Error writing on serial port, "
				"check device type and permissions.",
				"ERROR");

	e.Skip(true);
}

void frm_main::on_key_up(wxKeyEvent &e)
{
	static int c;

	c = e.GetKeyCode();
	switch(c)
	{
	case WXK_CONTROL:
		ctrl = false;
		/* no break */
	default:
		break;
	}
	e.Skip(true);
}

void frm_main::on_send(wxCommandEvent &)
{
	dlg_send ds;

	wxXmlResource::Get()->LoadDialog(&ds, this, _T("dlgSend"));

	ds.init();

	if (ds.ShowModal() != wxID_CANCEL) {
		string file = ds.get_file();

		if (file == "") {
			wxMessageBox("No file selected.", "ERROR");
			return;
		}

		/* disable sending and receiving from frm_main */
		detach_serial();
		rx_enabled = false;

		dispatcher *d = 0;

		switch(ds.get_proto()) {
		case p_raw:
			new RawSender(*com, this, file);
			return;
		case p_xmodem:
			d = new xmodem(*com, this, XMODEM_STD);
			break;
		case p_xmodem_crc:
			d = new xmodem(*com, this, XMODEM_CRC);
			break;
		case p_xmodem_1k:
			d = new xmodem(*com, this, XMODEM_1K);
			break;
		case p_ymodem:
			d = new ymodem(*com, this, YMODEM_STD);
			break;
		case p_zmodem:
			new zmodem(*com, this);
			break;
		}

		d->session_startup(file);
		d->start_transfert();
	}
}

void frm_main::on_close(wxCloseEvent &)
{
	wxConfigBase *c = wxConfig::Get();

	int x, y, w, h;

	GetSize(&w, &h);
	GetPosition(&x, &y);

	printf("saving pos %d %d\n", x, y);
	printf("saving size %d %d\n", w, h);

	c->SetPath("/Terminal");
	c->Write("wx", x);
	c->Write("wy", y);
	c->Write("wwidth", w);
	c->Write("wheight", h);

	c->Flush();

	/* clear serial and restore term */
	if (com) {
		delete(com);
		com = 0;
	}

	Destroy();
}

void frm_main::on_quit (wxCommandEvent &)
{
	detach_serial();
	Close();
}

void frm_main::on_clear (wxCommandEvent &)
{
	vte_terminal->clear();
}

void frm_main::on_reopen(wxCommandEvent &)
{
	init_terminal();
}

void frm_main::on_view_data(wxCommandEvent &e)
{
	int value = e.GetInt();

	mi_view_data->Check(value);
	tool_bar->ToggleTool(id_view_data, value);

	tool_bar->SetToolBitmap(id_view_data, value ?
		wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_view_data_enabled)) :
		wxBitmap(wxBITMAP_PNG_FROM_DATA(icon_view_data)));

	tool_bar->Realize();

	if (value) {
		detach_serial();
		tim.Start(20);
		Connect(tim.GetId(), wxEVT_TIMER,
			wxTimerEventHandler(frm_main::on_timer), NULL, this);

		vte_terminal->Connect(wxEVT_KEY_DOWN,
			   wxKeyEventHandler(frm_main::on_key_down),
				NULL, this);
		vte_terminal->Connect(wxEVT_KEY_UP,
			   wxKeyEventHandler(frm_main::on_key_up),
				NULL, this);
	} else {
		vte_terminal->Disconnect(wxEVT_KEY_DOWN,
				wxKeyEventHandler(frm_main::on_key_down));
		vte_terminal->Disconnect(wxEVT_KEY_UP,
				wxKeyEventHandler(frm_main::on_key_up));

		Disconnect(wxEVT_TIMER,
			    wxTimerEventHandler(frm_main::on_timer));
		attach_serial();
	}
}

void frm_main::on_pref(wxCommandEvent &)
{
	DlgPref dp;

	wxXmlResource::Get()->LoadDialog(&dp, this, _T("dlgPref"));

	dp.Init	();

	if (dp.ShowModal() != wxID_CANCEL)
	{
		get_com_cfg();

		/* update values here */
		init_terminal();
		init_terminal_window(false);
	}
}

void frm_main::on_custom_tool(wxCommandEvent &)
{
	DlgCustom dc;

	wxXmlResource::Get()->LoadDialog(&dc, this, _T("dlgCustom"));

	dc.Init();
	dc.SetSize(640, 480);

	if (dc.ShowModal() != wxID_CANCEL) {
		ccmds = dc.GetCmdVector();

		/* update values here */
		load_custom_tools();
	}
}

void frm_main::on_about(wxCommandEvent &)
{
	wxDialog da;

	wxXmlResource::Get()->LoadDialog(&da, this, _T("dlgAbout"));
	wxStaticText *stVersion = (wxStaticText*)
			XRCCTRL(da, "stVersion", wxStaticText);

	stVersion->SetLabel(wxString ("v.") + version);

	da.SetSize(300,340);
	da.CenterOnParent();

	da.ShowModal();
}

void frm_main::on_thread(wxCommandEvent & event)
{
	switch (event.GetId()) {
	case evt_id_log:
		vte_terminal->write((const char*)event.GetString().mb_str());
		break;
	case evt_id_del:
		vte_terminal->write("\x1b[1K\r");
		break;
	case evt_id_end:
		attach_serial();
		break;
	default:
		break;
	}
}

void frm_main::on_timer(wxTimerEvent &/*event*/)
{
	static int len, i;

	while ((len = com->read(&buff[0], max_buff)) > 0) {
		buff[len] = 0;
		for (i = 0; i < len; ++i) {
			if (view_data) {
				if ((buff[i] < 0x20 &&
					(buff[i] != 0x0d &&
					 buff[i] != 0x0a)) ||
					 buff[i] > 0x80) {
					vte_terminal->write_data_char(buff[i]);
					continue;
				}
			}
			vte_terminal->write(buff[i]);
		}
	}
}

void frm_main::on_timer_dev(wxTimerEvent&)
{
	static int modem_flags = -1;
	int new_flags;

	dev.get_device_states();
	update_serials_status();

	if (com) {
		new_flags = com->get_modem_flags();
		if (modem_flags != new_flags) {
			modem_flags = new_flags;
			update_dce_flags(modem_flags);
		}
	}
}

