/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "raw.hh"
#include <wx/string.h>
#include <fstream>
#include <sstream>

#include "serial.hh"

RawSender::RawSender (com_port &c, wxWindow *w, const string &f)
: thread((wxEvtHandler*)w), cp(c)
{
	fname = f;
	start();
}

wxThread::ExitCode RawSender::Entry()
{
	fstream f(fname.c_str(), fstream::in | fstream::binary);

	if (!f.is_open())
		return (wxThread::ExitCode)1;

	int size;
	stringstream ss;

	f.seekg(0, ios::end);
	size = f.tellg();
	f.seekg(0, ios::beg);

	/* non blocking write, we need to wait time of RX side */
	int delay = 1024*1000/(cp.get_speed_value()/11);
	/* add a safe offset */
	delay += 3;

	char p[1025];
	int i, count=0;

	evt_msg(wxString("\r\nFile: "+fname+"\r\n"));

	while (count<size) {
		f.read(p, 1024);
		i = f.gcount();
		cp.write(p, i);

		count += i;

		ss.clear();
		ss.str("");
		ss << count << "/" << size;

		evt_del ();
		evt_msg (ss.str());

		Sleep(delay);
	}
	evt_msg("\r\nTransfert terminated!\r\n");
	evt_end();

	return 0;
}
