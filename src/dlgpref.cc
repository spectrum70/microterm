/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#include "dlgpref.hh"

#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/property.h>
#include <wx/propgrid/advprops.h>
#include <wx/xrc/xmlres.h>
#include <wx/config.h>

BEGIN_EVENT_TABLE( DlgPref, wxDialog )
        EVT_BUTTON     ( wxID_OK, DlgPref::OnOk )
END_EVENT_TABLE()

DlgPref::DlgPref ()
{
}

const char *baud[] = {
        "200",
	"300",
	"600",
	"1200",
	"2400",
	"4800",
	"9600",
	"19200",
	"38400",
	"57600",
	"115200",
	"230400"
};
const int sz_baud = 12;

const char *parity[] = {
        "Odd",
	"Even",
	"None",
	0
};
const int sz_parity = 3;

void DlgPref::Init ()
{
	wxPanel *p = (wxPanel*) XRCCTRL(*this, "paGrid", wxPanel);

	pg = new wxPropertyGrid(p, -1);

	pg->InitAllTypeHandlers();

	wxPropertyGrid::AutoGetTranslation(true);

	SetSize(400,400);

	pg->SetSize(p->GetSize());

	pg->Append(new wxPropertyCategory("Terminal"));

	pg->Append(new wxColourProperty("Backgorund color", "bgcol"));
	pg->Append(new wxColourProperty("Foreground color", "fgcol"));
	pg->Append(new wxFontProperty("Font", "font"));
	pg->Append(new wxIntProperty("Buffer level", "bufle"));

	pg->Append(new wxPropertyCategory("Info bar"));

	pg->Append(new wxColourProperty("Background color",
					"ib_bgcol"));
	pg->Append(new wxColourProperty("Background color new",
					"ib_bgcol_new"));
	pg->Append(new wxColourProperty("Foreground color new",
					"ib_fgcol_new"));
	pg->Append(new wxColourProperty("Background color owned",
					"ib_bgcol_owned"));
	pg->Append(new wxColourProperty("Foreground color owned",
					"ib_fgcol_owned"));
	pg->Append(new wxColourProperty("Background color available",
					"ib_bgcol_avail"));
	pg->Append(new wxColourProperty("Foreground color available",
					"ib_fgcol_avail"));
	pg->Append(new wxColourProperty("Background color section",
					"ib_bgcol_sect"));
	pg->Append(new wxColourProperty("Foreground color section",
					"ib_fgcol_sect"));

	pg->Append(new wxPropertyCategory("Communication"));

	pg->Append(new wxStringProperty("Serial port", "devse"));
	pg->Append(new wxEnumProperty("Baud rate", "baudr",
	                wxArrayString(sz_baud, baud)));
	pg->Append(new wxEnumProperty("Parity", "parit",
	                wxArrayString(sz_parity, parity)));
	pg->Append(new wxIntProperty("Data bits", "datab"));
	pg->Append(new wxIntProperty("Stop bits", "stopb"));

	SetupValues();

	CenterOnParent();
}

wxFont DlgPref::GetFontFromConfig(const wxString &cfgstr)
{
	wxStringTokenizer tokenizer(cfgstr, ";");
	wxArrayString a;
	wxString str;

	while((str = tokenizer.GetNextToken()) != "")
		a.Add(str);

	wxString font_sys_name = a[1] + " " + a[3] + " " + a[0];

	return wxFont(font_sys_name);
}

void DlgPref::SetupValues ()
{
        wxConfig::Get()->SetPath("/Terminal");

        wxVariant v;

        wxColor b(wxConfig::Get()->Read("bgcol"));
        v << b;
        pg->SetPropertyValue("bgcol", v);

        wxColor f(wxConfig::Get()->Read("fgcol"));
        v << f;
        pg->SetPropertyValue("fgcol", v);

	wxFontProperty *fp = new wxFontProperty("Font", "font",
			GetFontFromConfig(wxConfig::Get()->Read("font")));
	pg->ReplaceProperty("font", fp);

        pg->SetPropertyValue("bufle", wxConfig::Get()->Read("bufle"));
        pg->SetPropertyAttribute ("bufle", wxPG_ATTR_MAX, 100000);
        pg->SetPropertyAttribute ("bufle", wxPG_ATTR_MIN, 1000);

	wxConfig::Get()->SetPath("/Info bar");

	pg->SetPropertyValue("ib_bgcol", wxConfig::Get()->Read("ib_bgcol"));
	pg->SetPropertyValue("ib_bgcol_new",
			     wxConfig::Get()->Read("ib_bgcol_new"));
	pg->SetPropertyValue("ib_fgcol_new",
			     wxConfig::Get()->Read("ib_fgcol_new"));
	pg->SetPropertyValue("ib_bgcol_owned",
			     wxConfig::Get()->Read("ib_bgcol_owned"));
	pg->SetPropertyValue("ib_fgcol_owned",
			     wxConfig::Get()->Read("ib_fgcol_owned"));
	pg->SetPropertyValue("ib_bgcol_avail",
			     wxConfig::Get()->Read("ib_bgcol_avail"));
	pg->SetPropertyValue("ib_fgcol_avail",
			     wxConfig::Get()->Read("ib_fgcol_avail"));
	pg->SetPropertyValue("ib_bgcol_sect",
			     wxConfig::Get()->Read("ib_bgcol_sect"));
	pg->SetPropertyValue("ib_fgcol_sect",
			     wxConfig::Get()->Read("ib_fgcol_sect"));

        wxConfig::Get()->SetPath("/Communication");

        pg->SetPropertyValue("devse", wxConfig::Get()->Read("devse"));
        pg->SetPropertyValue("baudr", wxConfig::Get()->Read("baudr", 6));
        pg->SetPropertyValue("parit", wxConfig::Get()->Read("parit", 2));
        pg->SetPropertyValue("datab", wxConfig::Get()->Read("datab"));
        pg->SetPropertyAttribute ("datab", wxPG_ATTR_MAX, 8);
        pg->SetPropertyAttribute ("datab", wxPG_ATTR_MIN, 5);
        pg->SetPropertyValue("stopb", wxConfig::Get()->Read("stopb"));
        pg->SetPropertyAttribute ("stopb", wxPG_ATTR_MAX, 2);
        pg->SetPropertyAttribute ("stopb", wxPG_ATTR_MIN, 1);
}

void DlgPref::OnOk (wxCommandEvent &)
{
	wxVariant v;
	wxColour c;

	if (pg->ClearSelection(true) == false)
		return;

	wxConfig::Get()->SetPath("/Terminal");

	v = pg->GetPropertyValue("bgcol"); c << v;
	wxConfig::Get()->Write("bgcol",c.GetAsString());

	v = pg->GetPropertyValue("fgcol"); c << v;
	wxConfig::Get()->Write("fgcol",c.GetAsString());

	wxString font = pg->GetPropertyValueAsString("font");

	wxConfig::Get()->Write("font", font);

	wxConfig::Get()->Write("bufle", pg->GetPropertyValueAsLong("bufle"));

	wxConfig::Get()->SetPath("/Info bar");

	v = pg->GetPropertyValue("ib_bgcol"); c << v;
	wxConfig::Get()->Write("ib_bgcol",c.GetAsString());
	v = pg->GetPropertyValue("ib_bgcol_new"); c << v;
	wxConfig::Get()->Write("ib_bgcol_new",c.GetAsString());
	v = pg->GetPropertyValue("ib_fgcol_new"); c << v;
	wxConfig::Get()->Write("ib_fgcol_new",c.GetAsString());
	v = pg->GetPropertyValue("ib_bgcol_owned"); c << v;
	wxConfig::Get()->Write("ib_bgcol_owned",c.GetAsString());
	v = pg->GetPropertyValue("ib_fgcol_owned"); c << v;
	wxConfig::Get()->Write("ib_fgcol_owned",c.GetAsString());
	v = pg->GetPropertyValue("ib_bgcol_avail"); c << v;
	wxConfig::Get()->Write("ib_bgcol_avail",c.GetAsString());
	v = pg->GetPropertyValue("ib_fgcol_avail"); c << v;
	wxConfig::Get()->Write("ib_fgcol_avail",c.GetAsString());
	v = pg->GetPropertyValue("ib_bgcol_sect"); c << v;
	wxConfig::Get()->Write("ib_bgcol_sect",c.GetAsString());
	v = pg->GetPropertyValue("ib_fgcol_sect"); c << v;
	wxConfig::Get()->Write("ib_fgcol_sect",c.GetAsString());

	wxConfig::Get()->SetPath("/Communication");

	wxConfig::Get()->Write("devse", pg->GetPropertyValueAsString("devse"));
	wxConfig::Get()->Write("baudr", pg->GetPropertyValueAsLong("baudr"));
	wxConfig::Get()->Write("parit", pg->GetPropertyValueAsLong("parit"));
	wxConfig::Get()->Write("datab", pg->GetPropertyValueAsLong("datab"));
	wxConfig::Get()->Write("stopb", pg->GetPropertyValueAsLong("stopb"));

	wxConfig::Get()->Flush();

	EndModal(wxID_OK);
}
