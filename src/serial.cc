/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "serial.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cerrno>
#include <wx/config.h>

#include <fcntl.h>		/* File control definitions */

using std::string;
using std::stringstream;

constexpr int fd_invalid = -1;

/* clone fd, allows read and write */
com_port::com_port(com_port &c) : fd(fd_invalid)
{
	if (c.get_fd() != fd_invalid)
		fd = dup(c.get_fd());
}

com_port::~com_port()
{
	if (fd != fd_invalid) {
		tcsetattr(fd, TCSANOW, &old_term);
		close(fd);
	}
}

com_port::com_port(int number, com_speed speed, com_parity parity,
		   int data_bits, int stop_bits) : fd(fd_invalid)
{
	stringstream port;

	port << "/dev/ttyS";
	port << number;

	init_port(port.str().c_str(), speed, parity, data_bits, stop_bits);
}

com_port::com_port(const char *port, com_speed speed, com_parity parity,
				int data_bits, int stop_bits)
{
	init_port(port, speed, parity, data_bits, stop_bits);
}
/**
 * This function open the port in fully raw mode, to send binary data also.
 */
void com_port::init_port(const char *port, com_speed speed, com_parity parity,
			 int data_bits, int stop_bits)
{
	/* store speed val */
	speed_val = speed_to_int(speed);

	wxConfig::Get()->SetPath("/Terminal");

	fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == fd_invalid) {
		stringstream msg;

		msg << "ComPort::" << __func__
		    << " unable to open port: " << port;

		perror(msg.str().c_str());

		return;
	} else {
		/* read return immediately (non-blocking) */
		fcntl(fd, F_SETFL, FNDELAY);

		struct termios options;

		tcgetattr(fd, &options);

		memcpy(&old_term, &options, sizeof(struct termios));
		memset(&options, 0, sizeof(struct termios));

		int s = translate_speed(speed);

		cfsetispeed(&options, s);
		cfsetospeed(&options, s);

		options.c_cflag |= (CLOCAL | CREAD);
		options.c_cflag &= ~CSIZE;
		// no flow control
		options.c_cflag &= ~CRTSCTS;
		options.c_cflag &= ~translate_parity(parity);

		(stop_bits>=2) ? options.c_cflag |= CSTOPB :
				 options.c_cflag &= ~CSTOPB;

		switch (data_bits) {
		case 5:	options.c_cflag |= CS5;	break;
		case 6:	options.c_cflag |= CS6;	break;
		case 7:	options.c_cflag |= CS7;	break;
		case 8:
		default: options.c_cflag |= CS8; break;
		}

		options.c_cflag |= CREAD;

		// turn off s/w flow ctrl
		options.c_iflag = IGNPAR | IGNBRK;

		options.c_cc[VMIN] = 1;
		options.c_cc[VTIME] = 0;

		options.c_lflag = 0;
		options.c_oflag = 0;

		tcsetattr(fd, TCSANOW, &options);
		tcflush(fd, TCIFLUSH);
		tcflush(fd, TCOFLUSH);
	}
}

void com_port::send_break ()
{
	if (fd != fd_invalid)
		tcsendbreak(fd, 0);
}

bool com_port::toggle_dtr()
{
	if (fd != fd_invalid)
	{
		int bits;

		ioctl(fd, TIOCMGET, &bits);
		(bits & TIOCM_DTR) ? bits &= ~TIOCM_DTR : bits |= TIOCM_DTR;
		ioctl(fd, TIOCMSET, &bits);
		return bits & TIOCM_DTR;
	}
	return false;
}

bool com_port::toggle_rts()
{
	if (fd != fd_invalid) {
		int bits;

		ioctl(fd, TIOCMGET, &bits);
		(bits & TIOCM_RTS) ? bits &= ~TIOCM_RTS : bits |= TIOCM_RTS;
		ioctl(fd, TIOCMSET, &bits);
		return bits & TIOCM_RTS;
	}
	return false;
}

int com_port::get_modem_flags()
{
	int bits = 0;

	if (fd != fd_invalid)
		ioctl(fd, TIOCMGET, &bits);

	return bits;
}

int com_port::translate_speed (com_speed s)
{
#ifndef WIN32
	switch (s) {
	case CS200:	return B200;
	case CS300:	return B300;
	case CS600:	return B600;
	case CS1200:	return B1200;
	case CS2400:	return B2400;
	case CS4800:	return B4800;
	case CS9600:	return B9600;
	case CS19200:	return B19200;
	case CS38400:	return B38400;
	case CS57600:	return B57600;
	case CS115200:	return B115200;
	case CS230400: return B230400;
	default: return 0;
	}
#else
	/* TO DO */
#endif
}

string com_port::get_speed_as_string(com_speed s)
{
	switch (s) {
	case CS200:	return "200";
	case CS300:	return "300";
	case CS600:	return "600";
	case CS1200:	return "1200";
	case CS2400:	return "2400";
	case CS4800:	return "4800";
	case CS9600:	return "9600";
	case CS19200:	return "19200";
	case CS38400:	return "38400";
	case CS57600:	return "57600";
	case CS115200:	return "115200";
	case CS230400:	return "230400";
	default: return "";
	}
	return "";
}

string com_port::get_parity_as_string(com_parity p)
{
        switch (p) {
	case 0: return "O";
	case 1: return "E";
	case 2: return "N";
        }
        return "";
}

int com_port::translate_parity(com_parity p)
{
	switch (p) {
	case CPODD:	return PARODD;
	case CPEVEN:	return PARENB;
	case CPNONE:
	default: return 0;
	}
}

int com_port::write(const char *buff, int len)
{
	int w;

	if (fd == fd_invalid)
		return 0;

	do {
		w = ::write(fd, buff, len);
		if (w >= 0) {
			len -= w;
			buff += w;

			fsync(fd);
		}
		else {
			switch (errno) {
			default:
			/* serial port exist but not writeable */
			case EIO: return -1;
			}
		}
	} while (len > 0);

	return 1;
}

int com_port::read(char *buff, int max_size)
{
	if (fd == fd_invalid)
		return 0;

	return ::read(fd, buff, max_size);
}
