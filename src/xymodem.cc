/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "xymodem.hh"
#include "serial.hh"
#include <iomanip>

#include <cstring>
#include <sstream>
#include <wx/string.h>

using namespace std;

static const unsigned char ZPAD = 0x2a;
static const unsigned char ZDLE = 0x18;
static const unsigned char ZBIN = 0x41;
static const unsigned char ZHEX = 0x42;
static const unsigned char XON  = 0x11;

static const unsigned char ZMABORT[] = {ZDLE,ZDLE,ZDLE,ZDLE,ZDLE};

static const unsigned char ESC_HEXHDR[] = {ZPAD,ZPAD,ZDLE,ZHEX};
static const unsigned char ZRQINIT[] = "0000000000195E";
static const unsigned char ZTERM[]= {0x0d,0x0a,XON};

class __tools {
public:
   __tools () {}

public:
	uint16_t crc16_ccitt( char *buf, int len )
	{
		uint16_t crc = 0;

		while (len--) {
			int i;
			crc ^= *(char *)buf++ << 8;
			for(i = 0; i < 8; ++i) {
				if( crc & 0x8000 )
					crc = (crc << 1) ^ 0x1021;
				else
					crc = crc << 1;
			}
		}

		return crc;
	}
	uint8_t calc_xmodem_checksum (unsigned char *buf, int sz)
	{
		int i;
		int cks = 0;

		for (i = 0; i < sz; ++i) {
			cks += (int)buf[i];
		}

		return cks % 256;
	}
};

generic_proto::generic_proto() : t(new __tools) {}
generic_proto::~generic_proto() { delete t; }

/* XMODEM */
static const unsigned char SOH = 0x01;
static const unsigned char STX = 0x02;
static const unsigned char EOT = 0x04;
static const unsigned char ACK = 0x06;
static const unsigned char NAK = 0x15;
static const unsigned char ETB = 0x17;
static const unsigned char CAN = 0x18;
static const unsigned char SYN = 0x43;
static const unsigned char CPMEOF = 0x1a;

dispatcher::dispatcher(com_port &c, wxEvtHandler *eh) : thread(eh), com(&c)
{
	obuff.resize(512 , 0);
	obuff.resize(2048, 0);
}

void dispatcher::session_startup(const string &file)
{
	fname = file;

	f.open(file.c_str(), fstream::binary | fstream::in);

	/* adding a cr, C was probably on the screen */
	if (!f.is_open()) {
		evt_msg(wxString("\r\nCannot open file " + file +
			", please check the path.\r\n"));
	}
	evt_msg(wxString("\r\nFile: "+file+"\r\n"));

	/* getting file size */
	f.seekg (0, ios::end);
	fsize = f.tellg();
	f.seekg (0, ios::beg);

	stringstream ss;
	string size;

	ss << fsize;
	ss >> size;
	evt_msg(wxString("Size: "+size+" bytes.\r\n"));
}

void dispatcher::start_transfert()
{
	start();

	evt_msg("Starting file transfert ...\r\n");
}

/*
 * Unified send block.
 */
void dispatcher::send_block(char *block, int size)
{
	char *p = (char *)obuff.c_str();
	char *s = p;
	int csz;

	/*
	 * Packet 1024 ? If yes, so STX, otherwise SOH
	 */
	*p++ = (size == LEN_B_XMODEM_1K) ? STX : SOH;
	*p++ = pkt;
	*p++ = 0xff - pkt;

	/* prepare block binary data */
	memcpy(p, block, size);
	p += size;
	csz = setup_checksum_crc(s + LEN_HDR, p, size);

	/* Final write */
	q->write(s, LEN_HDR + size + csz);
}

/*
 * A thread process the entire transfert and allows UI to be free of operating
 */
enum states {
	SESSION_START,
	SESSION_BLOCKS,
	SESSION_CLOSE,
	SESSION_CLOSE_WAIT_ACK,
	SESSION_END,
	SESSION_DBG,
};

bool dispatcher::get_char()
{
	if (q->read(&ibuff[0], 1) == 1)
		return true;
	else
		return false;
}

bool dispatcher::is_char ()
{ return (ibuff[0] != 0); }

bool dispatcher::is_ack()
{
	return (ibuff[0] == get_ack());
}

bool dispatcher::is_sync()
{
	return (ibuff[0] == get_sync());
}

bool dispatcher::is_nack()
{
	return (ibuff[0] == get_nack());
}

bool dispatcher::is_can()
{
	return (ibuff[0] == get_can());
}

void dispatcher::send_eot()
{
	char c[2];

	c[0] = get_eot();
	q->write(c, 1);
}

void dispatcher::setup_info_block (string &block)
{
	stringstream ss;

	unsigned int i = fname.rfind('/');

	if (i == string::npos)
		i = fname.rfind('\\');

	if (i != string::npos)
		fname = fname.substr(i+1);

	i = fname.size();

	ss << fsize;
	pkt = 0;

	/* prepare first block */
	memcpy(&block[0], &fname[0], i);
	block[i++] = 0;
	memcpy(&block[i], ss.str().c_str(), ss.str().size());
	/*
	 * space after length should be needed only if mod date is sent
	 * but some receiver (u-boot) need it.
	 */
	block[i + ss.str().size()] = ' ';
}

void dispatcher::update_progress(int size)
{
	stringstream ss;

	if (!size) {
		psize = get_block_size() * count++;
	} else
		psize += size;

	ss << psize << "/" << fsize;

	evt_del();
	evt_msg(ss.str());
}

void dispatcher::print_transfer_info()
{
	stringstream ss;

	evt_msg("Transfer start, protocol ");
	switch (get_proto()) {
		case XMODEM_STD:
			evt_msg("x-modem, ");
		break;
		case XMODEM_CRC:
			evt_msg("x-modem-crc, ");
		break;
		case XMODEM_1K:
			evt_msg("x-modem-1k, ");
		break;
		case YMODEM_STD:
			evt_msg("y-modem, ");
	}

	evt_msg("crc is ");

	if ((get_proto() & XMODEM_CRC) ||
		(get_proto() & XMODEM_1K) ||
		(get_proto() & YMODEM_STD))
		evt_msg("on, ");
	else
		evt_msg("off, ");

	ss << get_block_size();
	evt_msg(wxString("block size is ") + ss.str() + "\r\n");
}

wxThread::ExitCode dispatcher::Entry()
{
	string block(1024,0);

	struct y_modem_data ymd;

	int state = SESSION_START;
	int blnum = fsize / get_block_size();
	int reminder = 0;

	count = 1;
	dpg_ptr = 0;

	/* Progress size. */
	psize = 0;

	/* not cloning from gtk3 */
	q = com;

	pkt = 1;

	print_transfer_info();

	if (get_proto() == YMODEM_STD) {
		/*
		 * Always sending file size allows a faster close of
		 * the session from the receiver.
		 *
		 * = SPEC =
		 * The pathname (conventionally, the file name) is sent as
		 * a null terminated ASCII string.
		 * No spaces are included in the pathname.  Normally only the
		 * file name stem (no directory prefix) is transmitted unless
		 * the sender has selected YAM's f option to send the full
		 * pathname.  The source drive (A:, B:, etc.) is not sent.
		 */
		setup_info_block(block);
		blnum++;

		/* Signal to skip update progress for first info block. */
		ymd.first_pkt = true;
	}
	else
		/* Read first block from file. */
		f.read(&block[0], get_block_size());

	for (;;) {
		/* always, wait for tranfert occour and proper replies */
		Sleep(10);
		if (get_char() == false)
			continue;

		switch (state) {
		case SESSION_START:
			if (is_sync()) {
				int size = get_block_size();
				if (ymd.first_ack) {
					evt_dbg("\r\nEntry() : "
					       "YMODEM FIRST C after ACK");
					/*
					 * YMODEM, as per spec we expect
					 * receiver sends this further 'C'
					 */
					state++;
					/*
					 * clear rx data for walkthrough and
					 * send 1st block
					 */
					ibuff[0] = 0;
				} else {
					/* received SYN */
					evt_msg("Sync received\r\n");

					if (get_proto() == XMODEM_STD) {
						/*
						 * C here mean receiver wants
						 * 16bit CRC mode and assumes
						 * we know it if we send
						 * a block
						 */
						evt_msg("Setting "
							"CRC mode\r\n");
						set_mode(XMODEM_CRC);
						/* send and promote */
						state++;
					} else if (get_proto() == YMODEM_STD) {
						/* Only for file info block */
						size = 128;
					}

					Sleep(10);
					send_block(&block[0], size);

					continue;
				}
			} else if (is_ack()) {
				if (get_proto() == YMODEM_STD) {
					evt_dbg("YMODEM FIRST ACK\r\n");
					ymd.first_ack = true;
					/* back up to get C after ack */
					continue;
				}
				else
					state++;
			} else if (is_nack()) {
				/*
				* note ! programs as rx or lrz need filename as
				* additional parameter, otherwise they sends
				* C.. C and a CAN just after.
				*/
				if (get_proto() == XMODEM_STD) {
					evt_msg("Nack received, transfer "
							"start\r\n");
					/* resend same initial packet*/
					send_block(&block[0], get_block_size());
					Sleep(10);
					/* promoted */
					state++;
				} else {
					evt_dbg("\r\nEntry() : NACK FIRST");
				}
			}
			/* no break */
		case SESSION_BLOCKS:
			if (is_char() && !is_ack()) {
				if (is_sync()) {
					if (get_proto() == YMODEM_STD) {
						/*
						* first synch after first
						* block sent, do nothing here
						*/
					}
				} else if (is_nack()) {
					evt_msg("Nack :( has proper transfer "
						"type been selected ?\r\n");
					/* resend */
					if (get_proto() == YMODEM_STD ||
					   (get_proto() == XMODEM_STD
							   && pkt != 1))
					{
						evt_msg("\r\n entry() : "
							"NACK, RESEND \r\n");
						send_block (&block[0],
							get_block_size());
						Sleep(10);
					}
				} else if (is_can()) {
					evt_msg("\r\nTransfert canceled "
						"from receiver\r\n");
					Sleep(100);
					/* destroy object and the thread */
					delete q;
					evt_end();
					return (void*)1;
				} else {
					/* debug chars sent back */
					state = SESSION_DBG;
				}
				continue;
			}
			// ACK received, so progress
			if (blnum) {
				if (get_proto() == YMODEM_STD && ymd.first_pkt)
					ymd.first_pkt = false;
				else
					update_progress();

				/* if multiple of 256/1024 close */
				if (blnum == 1 && psize == fsize) {
					blnum--;
					goto close_session;
				}
			} else {
close_session:
				/* completed */
				update_progress (reminder);
				evt_msg ("\r\nClosing session ...\r\n");
						send_eot();
				if (get_proto() == XMODEM_STD ||
					get_proto() == XMODEM_CRC)
					state = SESSION_END;
				else
					state++;
				continue;
			}
			blnum--;
			pkt++;
			/* end of blocks ? */
			if (blnum > 0) {
				f.read(&block[0], get_block_size());
				send_block(&block[0], get_block_size());
			} else {
				reminder = fsize % get_block_size();
				if (reminder) {
					f.read(&block[0], reminder);
					memset(&block[reminder],
							CPMEOF,
							get_block_size() -
							reminder);
					send_block(&block[0],
							get_block_size());
				}
			}
			break;
		case SESSION_CLOSE:
			/* YMODEM ONLY */
			if (is_ack())
				state++;
			else if (is_nack()) {
				/*
				 * in YMODEM termination is EOT-> NACK<-,
				 * EOT-> ACK<-
				 */
				send_eot();
			}
			break;
		case SESSION_CLOSE_WAIT_ACK:
			/* YMODEM ONLY */
			if (is_sync()) {
				/* Synch, terminating.. */
				memset (&block[0], 0, 128);
				set_size(128);
				pkt = 0;
				send_block (&block[0], 128);

				state++;
			}
			break;
		case SESSION_END:
			if (!is_ack())
				continue;
			evt_msg("Transfert complete\r\n");
			Sleep(100);
			/* destroy object and the thread */
			//delete q;
			evt_end();
			return (void*)1;

			break;
		case SESSION_DBG: {
			/* everything here is traced */
			static stringstream ss;
			ss << hex
				<< setw(2) << setfill('0')
				<< (int)((uint8_t)ibuff[0] & 0xff) << " ";

			if (!(++dpg_ptr % 16)) {
				evt_msg(ss.str());
				evt_msg("\r\n");
				ss.clear();
				ss.str("");
			}

			break;
		}
		}
	}

	return 0;
}

xmodem::xmodem(com_port &c, wxEvtHandler *eh, int proto_variant)
: dispatcher(c, eh)
{
	proto = proto_variant;

	if (proto == XMODEM_1K) {
		blk_size = LEN_B_XMODEM_1K;
	} else {
		blk_size = LEN_B_XMODEM;
	}
}

int xmodem::setup_checksum_crc(char *data, char *dest, int size)
{
	if (proto == XMODEM_STD) {
		unsigned char crc =
			t->calc_xmodem_checksum(
					(unsigned char*)data, size);

		*dest = crc;

		return 1;
	} else {
		unsigned short crc = t->crc16_ccitt(data, size);

		*(unsigned short*)dest = (unsigned short)
			((crc << 8) & 0xFF00) | ((crc >> 8) & 0xff);

		return 2;
	}
}

inline char xmodem::get_sync() { return SYN; }
inline char xmodem::get_ack() { return ACK; }
inline char xmodem::get_nack() { return NAK; }
inline char xmodem::get_can() { return CAN; }
inline char xmodem::get_eot() { return EOT; }

inline void xmodem::set_size (int size) { blk_size = size; }

ymodem::ymodem (com_port &c, wxEvtHandler *eh, int proto_variant)
: dispatcher(c, eh)
{
	proto = proto_variant;
	blk_size = LEN_B_YMODEM;
}

int ymodem::setup_checksum_crc(char *data, char *dest, int size)
{
	unsigned short crc = t->crc16_ccitt(data, size);

	*(unsigned short*)dest = (unsigned short)((crc << 8) & 0xFF00)
					| ((crc >> 8) & 0xff);

	return 2;
}

inline char ymodem::get_sync() { return SYN; }
inline char ymodem::get_ack() { return ACK; }
inline char ymodem::get_nack() { return NAK; }
inline char ymodem::get_can() { return CAN; }
inline char ymodem::get_eot() { return EOT; }

inline void ymodem::set_size (int size) { blk_size = size; }
