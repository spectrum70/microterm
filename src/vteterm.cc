/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "vteterm.hh"

#include <gtk/gtk.h>
#include <wx/window.h>
#include <sstream>

using std::stringstream;

extern "C" {
/*
 * callback for left button down on the terminal
 */
static gboolean gtk_button_press_callback(GtkRange *widget,
                                      GdkEventButton *gdk_evtbtn,
                                      wxControl *ctrl)
{
	switch (gdk_evtbtn->type) {
	case GDK_BUTTON_PRESS:
		/* right button */
		if (gdk_evtbtn->button == 3) {
			wxCommandEvent event(wxEVT_RIGHT_DOWN, ctrl->GetId());

			event.SetEventObject(ctrl);
			ctrl->GetEventHandler()->ProcessEvent(event);
		}
	default:
		break;
	}

	return FALSE;
}

} /* extern C */

vte_term::vte_term() : pty(0), h_key_press(0) {}

vte_term::vte_term(wxWindow *parent, wxWindowID id, const wxString &label,
			const wxPoint &pos, const wxSize &size, long style,
			const wxString &name) : pty(0), h_key_press(0)
{
        // reset colors
        f = b = 0;

        if (!PreCreation(parent, pos, size) ||
		!CreateBase(parent, id, pos, size, style,
			     wxDefaultValidator, name)) {
		wxFAIL_MSG( wxT("wxControl creation failed") );
		return;
	}

	term = (VteTerminal *)vte_terminal_new();

	GtkWidget *scrollbar;

	m_widget = gtk_hbox_new(0, 0);

        /* Create the scrollbar for the widget. */
        scrollbar = gtk_scrollbar_new(GTK_ORIENTATION_VERTICAL,
			gtk_scrollable_get_vadjustment((GtkScrollable*)term));

        gtk_box_pack_start(GTK_BOX(m_widget), (GtkWidget *)term, TRUE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(m_widget), scrollbar, FALSE, TRUE, 0);

	gtk_widget_show((GtkWidget *)term);
        gtk_widget_show(scrollbar);

	/* xterm only supported */
	vte_terminal_set_scroll_on_output(term, 0);
        vte_terminal_set_scroll_on_keystroke(term, 1);
        vte_terminal_set_scrollback_lines(term, 5000);
	vte_terminal_set_allow_bold(term, 1);

	g_signal_connect(term, "button_press_event",
				G_CALLBACK(gtk_button_press_callback),
				(gpointer)this);

	PostCreation(size);

	m_parent->DoAddChild(this);
}

void vte_term::set_buffer_lines(int lines)
{
        vte_terminal_set_scrollback_lines(term, lines);
}

void vte_term::DoSetSize(int x, int y,
                           int width, int height,
                           int sizeFlags )
{
	wxControl::DoSetSize( x, y, width, height, sizeFlags );
}

wxSize vte_term::DoGetBestSize() const
{
	GtkAllocation* alloc = g_new(GtkAllocation, 1);
	gtk_widget_get_allocation((GtkWidget *)term, alloc);

	wxSize sz(alloc->width+1, alloc->height);

	g_free(alloc);

	return sz;
}

/*
 * Note: len allows binary codes to be processed from the terminal.
 */
void vte_term::write(const string &text)
{
	vte_terminal_feed(term, text.c_str(), text.size());
}

void vte_term::write(const char *buff, int len)
{
	vte_terminal_feed(term, buff, len);
}

void vte_term::write(char d)
{
	vte_terminal_feed(term, &d, 1);
}

void vte_term::hide_cursor(bool hide)
{
	if (hide) {
		vte_terminal_feed(term, "\x1b[?25l", 6);
	} else {
		vte_terminal_feed(term, "\x1b[?25h", 6);
	}
}

void vte_term::char_to_hex(unsigned char d, char *rbuff)
{
	int i = d / 16;
	int m = d % 16;

	rbuff[0] = (i < 10) ? i + 48 : i + 87;
	rbuff[1] = (m < 10) ? m + 48 : m + 87;
}

void vte_term::write_data_char(unsigned char d)
{
	static char hexc[5] = {"[xx]"};

	vte_terminal_feed(term, "\x1b[7m", 4);
	char_to_hex(d,&hexc[1]);
	vte_terminal_feed(term, hexc, 4);

	vte_terminal_feed(term, "\x1b[0m", 4);
}

void vte_term::clear()
{
	vte_terminal_reset(term, 1, 1);
}

void vte_term::set_colours(const char *fg, const char *bg)
{
	if (f != 0) {
		delete(f);
		delete(b);
	}

	f = new GdkRGBA;
	b = new GdkRGBA;

	gdk_rgba_parse(b, bg);
	gdk_rgba_parse(f, fg);

	vte_terminal_set_colors(term, f, b, 0, 0);
	vte_terminal_set_color_bold(term, f);

	gtk_widget_realize((GtkWidget *)term);
}

void vte_term::set_weight_by_sgr(const string &weight)
{
	string sgr =  "\x1b[0;";

	if (weight == "Bold")
		sgr += "1m";

	vte_terminal_feed(term, sgr.c_str(), sgr.size());
}

/*
 * pango wants
 *
 * "[FAMILY-LIST] [STYLE-OPTIONS] [SIZE] [VARIATIONS]"
 *
 * styles: "Normal", "Roman", "Oblique", "Italic".
 * A typical example:
 * " Cantarell Italic Light 15 @wght=200"
 *
 * from config: [10; Inconsolata; Normal; Bold; Not Underlined; Teletype]
 *
 * NOTE:
 * vte_terminal guys decide to skip applying bold/weight, since they
 * consider this must be done by SGR ....
 * This complicates things, using set_weight_by_sgr().
 */
string vte_term::wx_font_string_to_pango(const string &f)
{
	string size;
	string family_name;
	string style = "";
	string weight = "";

        size_t i, l;
	if ((i = f.find(';')) != string::npos) {
		size = f.substr(0, i);

		if ((l = f.find(';', i + 1)) != string::npos)
			family_name = f.substr(i + 2, l - i - 2);

		i = l + 2;
		if ((l = f.find(';', i)) != string::npos)
			style = f.substr(i, l - i);

		i = l + 2;
		if ((l = f.find(';', i)) != string::npos)
			weight = f.substr(i, l - i);
        }

        set_weight_by_sgr(weight);

        /*
         * family name can be optionally terminated by a comma, let's use it to
         * allow family names containing spaces.
         */
        return family_name + " " + style + " " + weight + " " + size;
}

void vte_term::copy_to_clipboard ()
{
        vte_terminal_copy_clipboard(term);
}

void vte_term::paste_clipboard ()
{
        vte_terminal_paste_clipboard(term);
}

bool vte_term::has_selection()
{
        return vte_terminal_get_has_selection(term);
}

void vte_term::set_font (const string &f)
{
	string pango_str = wx_font_string_to_pango(f);

	vte_terminal_set_font(term,
		pango_font_description_from_string(pango_str.c_str()));
}

int vte_term::get_char_width()
{
        return vte_terminal_get_char_width(term);
}

int vte_term::get_char_height()
{
        return vte_terminal_get_char_height(term);
}

/*
 * waiting to be fixed, since selecting only visible lines
 */
void vte_term::select_all()
{
	vte_terminal_select_all(term);
}

