/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <wx/button.h>
#include <wx/textdlg.h>
#include <wx/msgdlg.h>
#include <wx/listbox.h>
#include <wx/stc/stc.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/xrc/xmlres.h>
#include <wx/settings.h>
#include <wx/config.h>
#include <wx/bmpbuttn.h>
#include <wx/msgdlg.h>

#include <iomanip>
#include <sstream>

#include "dlgcustom.hh"

using namespace std;

enum ids {
        id_command = 1000,
};

BEGIN_EVENT_TABLE( DlgCustom, wxDialog )
   EVT_BUTTON     ( wxID_OK, DlgCustom::OnOk )
   EVT_BUTTON     ( XRCID("btnNew"), DlgCustom::OnCommandNew )
   EVT_BUTTON     ( XRCID("btnDelete"), DlgCustom::OnCommandDelete )
   EVT_BUTTON     ( XRCID("btnUp"), DlgCustom::OnCommandUp )
   EVT_BUTTON     ( XRCID("btnDown"), DlgCustom::OnCommandDown )
   EVT_KILL_FOCUS ( DlgCustom::OnKillFocus )
   EVT_LISTBOX    ( XRCID("lstCmds"), DlgCustom::OnListBox )
END_EVENT_TABLE()

DlgCustom::DlgCustom()
{
        selitem = -1;
}

void DlgCustom::Init()
{
        btnNew = (wxButton*) XRCCTRL(*this, "btnNew", wxButton);
        btnDelete = (wxButton*) XRCCTRL(*this, "btnDelete", wxButton);
        btnUp = (wxBitmapButton*) XRCCTRL(*this, "btnUp", wxBitmapButton);
        btnDown = (wxBitmapButton*) XRCCTRL(*this, "btnDown", wxBitmapButton);

        btnUp->SetBitmapLabel(
                wxArtProvider::GetBitmap("gtk-go-up", wxART_MENU));
        btnDown->SetBitmapLabel(
                wxArtProvider::GetBitmap("gtk-go-down", wxART_MENU));

        btnUp->Disable();
        btnDown->Disable();

        lstCmds = (wxListBox*) XRCCTRL(*this, "lstCmds", wxListBox);

        wxPanel *pnl = (wxPanel*) XRCCTRL(*this, "pnlCommand", wxPanel);

        wxBoxSizer *sz = new wxBoxSizer(wxVERTICAL);

        pnl->SetSizer(sz);

        txtCommand = new wxStyledTextCtrl(pnl, id_command,
			wxDefaultPosition, wxDefaultSize);

	txtCommand->SetMarginLeft(5);
	txtCommand->SetMarginWidth(0, 40);
	txtCommand->SetMarginWidth(1, 0);
	txtCommand->StyleSetForeground(wxSTC_STYLE_LINENUMBER,
				       wxColour(45, 45, 45) );
        txtCommand->StyleSetBackground(wxSTC_STYLE_LINENUMBER,
				       wxColour(240, 240, 250));
	txtCommand->StyleSetBackground(wxSTC_STYLE_DEFAULT,
				       wxColour(255, 255, 255));
	txtCommand->StyleSetForeground(wxSTC_STYLE_BRACELIGHT,
				       wxColour(180, 180, 80));
	txtCommand->StyleSetForeground(wxSTC_STYLE_INDENTGUIDE,
				       wxColour(80, 80, 80));

        txtCommand->SetLexer(wxSTC_LEX_BASH);
	txtCommand->StyleSetForeground(wxSTC_SH_DEFAULT, wxColour(10, 10, 10));
        txtCommand->StyleSetForeground(wxSTC_SH_OPERATOR, wxColour(10, 10, 120));
	txtCommand->StyleSetForeground(wxSTC_SH_WORD, wxColour(120, 10, 120));
	txtCommand->StyleSetForeground(wxSTC_SH_NUMBER, wxColour(255, 10, 10));
	txtCommand->StyleSetForeground(wxSTC_SH_IDENTIFIER,
				       wxColour(40, 100, 20));
	txtCommand->StyleSetForeground(wxSTC_SH_STRING, wxColour(20, 20, 200));
	txtCommand->StyleSetForeground(wxSTC_SH_COMMENTLINE,
				       wxColour(20, 120, 120));
	txtCommand->SetUseTabs(true);
	txtCommand->SetHighlightGuide(true);
	txtCommand->SetIndentationGuides(true);
        txtCommand->SetIndent(8);

	txtCommand->SetKeyWords(0, "if then else fi echo while true");

	wxFont font(10, wxTELETYPE, wxNORMAL, wxNORMAL, false);
	txtCommand->StyleSetFont(wxSTC_STYLE_LINENUMBER, font);
	txtCommand->StyleSetFont(wxSTC_STYLE_DEFAULT, font);

        sz->Add(txtCommand, 1, wxEXPAND | wxALL);

        CenterOnParent();

        LoadCustomCommands();

        txtCommand->Enable(false);
}

void DlgCustom::OnListBox(wxCommandEvent &e)
{
        if (e.GetId()==XRCID("lstCmds")) {
                txtCommand->Enable(true);

                int si = lstCmds->GetSelection();

                btnUp->Disable();
                btnDown->Disable();

                int cnt = lstCmds->GetCount();

                if (si > 0) btnUp->Enable();
                if (si < (cnt-1)) btnDown->Enable();

                if (si != selitem) {

                        /* save the previous that can be edited */
                        if (selitem != -1) {
                                pair <string,string> p(
                                (const char*)lstCmds->GetString(selitem).mb_str(),
                                (const char*)txtCommand->GetValue().mb_str() );

                                cmds[selitem] = p;
                        }
                }
                // assume same list / vector ordering
                string n = (const char*)lstCmds->GetString(si).mb_str();

                txtCommand->SetValue(cmds[si].second);

                selitem = si;
        }
}

void DlgCustom::LoadCustomCommands()
{
        wxConfigBase &cfg = * wxConfig::Get();

        cfg.SetPath("/custom");

        wxString e, value;
        long i;

        if (cfg.GetFirstEntry(e, i)) {
                do {
                        pair<string,string> p;

                        string entry = (const char*)cfg.Read(e).mb_str();

                        int sep = entry.find(':');

                        string name = entry.substr(0,sep);
                        string cmd  = entry.substr(sep+1);

                        p.first  = name;
                        p.second = cmd;

                        cmds.push_back(p);

                        lstCmds->Append(name);
                }
                while (cfg.GetNextEntry(e, i));
        }
}

int DlgCustom::ValidateCmdName (const string &cmd)
{
        static const char invalid[] = {"@/\\*?:"};
        unsigned int i, q;

        for (i = 0; i < cmd.size(); ++i) {
                q = 0;
                while (*(invalid+q)) {
                        if (cmd[i]==*(invalid+q)) return 0;
                        q++;
                }
        }
        return 1;
}

void DlgCustom::OnCommandNew(wxCommandEvent &)
{
        wxString command = ::wxGetTextFromUser(
                "Enter the command name",
                "Custom Command");

        if (!ValidateCmdName((const char*)command.ToAscii())) {
                wxMessageDialog d(0, "Invalid char, @/\\*?: are not allowed.",
                                 "ERROR");
                d.ShowModal();

                return;
        }

        if (command != "") {

                pair<string,string> p;

                p.first  = command;
                p.second = "";

                cmds.push_back(p);

                int item = lstCmds->Append(command);

                lstCmds->Select(item);

                wxCommandEvent e(wxEVT_COMMAND_LISTBOX_SELECTED,
                                        XRCID("lstCmds"));
                ProcessEvent(e);
        }
}

void DlgCustom::OnCommandDelete(wxCommandEvent &)
{
        int item = lstCmds->GetSelection();

        if (item != wxNOT_FOUND) {
                // assume same pos as in the memory vector
                cmds.erase(cmds.begin()+item);

                lstCmds->Delete(item);

                if (!lstCmds->GetCount()) {

                        txtCommand->Enable(false);
                        txtCommand->SetBackgroundColour(wxColour("#c0c0c0"));

                        btnUp->Disable();
                        btnDown->Disable();

                        return;
                }

                if (selitem >= item) selitem--;

                int sel = (item == 0) ? 0 : item - 1;

                lstCmds->Select(sel);

                wxCommandEvent e(wxEVT_COMMAND_LISTBOX_SELECTED,
                                        XRCID("lstCmds"));
                ProcessEvent(e);
        }
}

void DlgCustom::MoveCommand (int delta)
{
        // get selected
        int si = lstCmds->GetSelection();

        /* dataswap */
        pair <string,string> p = cmds[si+delta];

        cmds[si+delta] = cmds[si];
        cmds[si]   = p;

        /* visual */
        lstCmds->Delete(si);
        lstCmds->Insert(cmds[si+delta].first, si+delta);

        selitem = si+delta;

        lstCmds->SetSelection(si+delta);

        wxCommandEvent e(wxEVT_COMMAND_LISTBOX_SELECTED,
                                        XRCID("lstCmds"));
        ProcessEvent(e);
}

void DlgCustom::OnCommandUp(wxCommandEvent &)
{
        MoveCommand (-1);
}

void DlgCustom::OnCommandDown(wxCommandEvent &)
{
        MoveCommand (+1);
}

void DlgCustom::OnKillFocus(wxFocusEvent &)
{
}

void DlgCustom::OnOk(wxCommandEvent &)
{
        wxConfigBase &cfg = * wxConfig::Get();

        /* always store current command */
        int si = lstCmds->GetSelection();

        if (si != wxNOT_FOUND) {
                wxString name = lstCmds->GetString(si);

                for (unsigned int i = 0; i < cmds.size(); ++i) {
                        pair <string,string> p(cmds[i]);

                        if (p.first == name) {
                                p.second = txtCommand->GetValue();
                                cmds[i] = p;

                                break;
                        }
                }
        }
        /* store values in config now */
        cfg.DeleteGroup("/custom");
        cfg.SetPath("/custom");

        vect_cmds::iterator i;

        int idx=0;

        for (i = cmds.begin(); i != cmds.end(); ++i, ++idx ) {

                stringstream ss, sc;

                ss << "cmd" << setw(2) << setfill('0') << idx;
                sc << i->first.c_str() << ":" << i->second.c_str();

                cfg.Write(ss.str().c_str(), sc.str().c_str());
        }

        cfg.Flush();

        EndModal (wxID_OK);
}
