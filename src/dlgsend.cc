/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include "dlgsend.hh"

#include <wx/xrc/xmlres.h>
#include <wx/html/htmlwin.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/filedlg.h>
#include <wx/settings.h>

#include <sstream>

using std::stringstream;

BEGIN_EVENT_TABLE(dlg_send, wxDialog)
	EVT_BUTTON(XRCID("btnFile"), dlg_send::on_select_file)
	EVT_CHOICE(XRCID("chProtocol"), dlg_send::on_protocol)
END_EVENT_TABLE()

void dlg_send::init()
{
	fname = "";
	proto = 0;

	btnFile = (wxButton*) XRCCTRL(*this, "btnFile", wxButton);
	btnStart = (wxButton*) XRCCTRL(*this, "btnStart", wxButton);
	btnCancel = (wxButton*) XRCCTRL(*this, "btnCancel", wxButton);
	chProtocol = (wxChoice*) XRCCTRL(*this, "chProtocol", wxChoice);
	hwFile = (wxHtmlWindow*) XRCCTRL(*this, "hwFile", wxHtmlWindow);

	string t = "<html><body><font size=\"2\" color=\"blue\">"
		"<p><br><br> &nbsp;&nbsp;please select a file ..."
		"</p></body></html>";
	hwFile->SetBorders(1);

	hwFile->SetPage(t);
	hwFile->Show();

	btnStart->SetId(wxID_OK);
	btnCancel->SetId(wxID_CANCEL);
}

void dlg_send::update_file_details()
{
	wxFile f(fname);

	stringstream ss;

	ss << f.Length();

	string name=fname;

#ifdef WIN32
	unsigned int i = fname.rfind('\\');
#else
	unsigned int i = fname.rfind('/');
#endif
	if (i!=string::npos) {
		name=name.substr(i+1);
	}

	string t =
		"<html><body bgcolor=\"#c0c0c0\">"
		"<font size=\"1\" color=\"blue\">";
	t += "<table border=0 cellspacing=1 cellpadding=1 width=100%>";
	t += "<tr bgcolor=\"white\"><td>File: </td><td>" + name;
	t += "<tr bgcolor=\"cyan\"><td>Length: </td><td>" + ss.str() + "bytes";
	t += "<tr bgcolor=\"white\"><td>Protocol: </td><td>" +
			chProtocol->GetStringSelection();

	hwFile->SetPage(t);
	hwFile->Show();
}

void dlg_send::on_select_file(wxCommandEvent &)
{
	wxFileDialog fd (this,
			 "Select upload file...",
			 "",
			 "",
			 "all files (*)|*"
			);

	if (fd.ShowModal()!=wxCANCEL) {
		fname = fd.GetPath();

		update_file_details ();

		btnStart->SetBackgroundColour("#ffff00");
	}
}

void dlg_send::on_protocol (wxCommandEvent &)
{
	proto = chProtocol->GetSelection();

	if (fname != "")
		update_file_details();
}
