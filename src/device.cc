/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <unistd.h>
#include <stdio.h>
#include <cstring>

#include "device.hh"

#define sysfs_path "/sys/class/tty/tty"

static char std_ser[32] = sysfs_path "S";
static char std_usb[32] = sysfs_path "USB";
static char std_acm[32] = sysfs_path "ACM";

device::device() {}

void device::set_in_use(enum types t, int num)
{
	if (t < 0 || t >= t_max || num > max_devs)
		return;

	state[t][num] |= s_owned;
}

void device::get_device_states()
{
	int i;

	memcpy(&state_old[0][0], &state[0][0], max_devs * 3);

	for (i = 0; i < max_devs; ++i) {
		std_ser[19] = i + 48;
		if (access(std_ser, F_OK) == 0) {
			if (!(state[t_serial][i] & s_exist)) {
				state[t_serial][i] |= (s_exist | s_new);
			} else {
				state[t_serial][i] &= ~s_new;
			}
		}
	}

	for (i = 0; i < max_devs; ++i) {
		std_usb[21] = i + 48;
		if (access(std_usb, F_OK) == 0) {
			if (!(state[t_usb][i] & s_exist)) {
				state[t_usb][i] |= (s_exist | s_new);
			} else {
				state[t_usb][i] &= ~s_new;
			}
		} else {
			state[t_usb][i] = 0;
		}
	}

	for (i = 0; i < max_devs; ++i) {
		std_acm[21] = i + 48;
		if (access(std_acm, F_OK) == 0) {
			if (!(state[t_acm][i] & s_exist)) {
				state[t_acm][i] |= (s_exist | s_new);
			} else {
				state[t_acm][i] &= ~s_new;
			}
		} else {
			state[t_acm][i] = 0;
		}
	}
}
