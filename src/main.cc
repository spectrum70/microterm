/*
 * microterm :)
 *
 * (C) Copyright 2011 Angelo Dureghello <angelo@kernel-space.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <wx/app.h>
#include <wx/stdpaths.h>
#include <wx/config.h>
#include <wx/xrc/xmlres.h>
#include <wx/msgdlg.h>

#include "frmmain.hh"

class microterm : public wxApp
{
public:
	virtual bool OnInit();

private:
	void check_config();
};

IMPLEMENT_APP(microterm)

void microterm::check_config()
{
	wxConfig *c = new wxConfig("microterm");

	if (!c->Exists("Terminal")) {
		/* setup defaults now */
		c->DeleteAll();

		c->SetPath("/Terminal");
		c->Write("fgcol", "rgb(220, 220, 220)");
		c->Write("bgcol", "rgb(0, 0, 0)");
		c->Write("font",
		      "10; Monospace; Normal; Bold; Not Underlined; Teletype");
		c->Write("bufle", 5000);
		c->Write("nl_to_cr", true);
		c->Write("wwidth", 640);
		c->Write("wheight", 480);

		c->SetPath("/Info bar");
		c->Write("ib_bgcol", "rgb(0, 0, 0)");
		c->Write("ib_bgcol_new", "rgb(255, 255, 0)");
		c->Write("ib_fgcol_new", "rgb(0, 0, 0)");
		c->Write("ib_bgcol_owned", "rgb(128, 128, 255)");
		c->Write("ib_fgcol_owned", "rgb(0, 0, 0)");
		c->Write("ib_bgcol_avail", "rgb(40, 40, 40)");
		c->Write("ib_fgcol_avail", "rgb(200, 200, 0)");
		c->Write("ib_bgcol_sect", "rgb(200, 200, 0)");
		c->Write("ib_fgcol_sect", "rgb(0, 0, 0)");

		c->SetPath("/Communication");
		c->Write("devse", "/dev/ttyS0");
		c->Write("baudr", 10);
		c->Write("parit", 2);
		c->Write("datab", 8);
		c->Write("stopb", 1);

		c->Flush();
	}
}

constexpr int MAX_PATHS = 8;
constexpr int MAX_PATH_LEN = 128;

static const char paths[MAX_PATHS][MAX_PATH_LEN] = {
	{"/usr/share/microterm/"},
	{"/usr/local/share/microterm/"},
	{0}
};

bool microterm::OnInit()
{
	string resource, icon;
	int i;

	wxStandardPaths & sp = wxStandardPaths::Get();
	wxString path = sp.GetExecutablePath();

	path=path.substr(0,path.rfind('\\'));

	check_config();

	for (i = 0; i < MAX_PATHS; ++i) {
		resource = paths[i];
		if (wxFileExists(resource+"microterm.xrc"))
			break;
	}

	if (i == MAX_PATHS) {
		resource = path + "/res/";

		if (!wxFileExists(resource+"microterm.xrc")) {
			string msg;

			msg += "Resource file missing\n"
				 "check for microterm.xrc to exist, as\n"
				 "./res/microterm.xrc\n";

			for (i = 0; (i<MAX_PATHS) && (paths[i][0]!=0); ++i)
			{
				msg += paths[i];
				msg += "\n";
			}

			wxMessageDialog(0, msg, "ERROR", wxOK | wxICON_ERROR).
					ShowModal();

			return false;
		}
	}

	wxXmlResource::Get()->InitAllHandlers();
	wxXmlResource::Get()->Load(resource + "microterm.xrc");
	wxInitAllImageHandlers();

	frm_main *f = new frm_main;
	f->SetIcon(wxIcon(resource + "/microterm.png", wxBITMAP_TYPE_PNG));
	f->Show(TRUE);
	SetTopWindow(f);

	return TRUE;
}
